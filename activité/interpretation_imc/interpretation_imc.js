let poids = 100;
let taille = 1.75;
let interpretation ;

let imc = poids / ( taille * taille);

console.log("poids = " + poids + " kg");
console.log("taille = " + taille + " m");
console.log("imc = " + imc);

if (imc < 16.5) {
    interpretation = 'Dénutrition';
}else if(imc >= 16.5 && imc < 18.5){
    interpretation = 'Maigreur';
}else if(imc >= 18.5 && imc < 25){
    interpretation = 'Corpulence normale';
}else if(imc >= 25 && imc < 30){
    interpretation = 'Surpoids';
}else if(imc >= 30 && imc < 35){
    interpretation = 'Obésité modérée';
}else if(imc >= 35 && imc <= 40){
    interpretation = 'Obésité sévère';
}else if(imc > 40){
    interpretation = 'Obésité morbide';
}

console.log("interpretation de l IMC : " + interpretation);