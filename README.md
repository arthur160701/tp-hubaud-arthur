# tp-hubaud-arthur

Dossier TP , Gustave Eiffel

## Activitée

* [Calcul de surface](/activité/Calcul_Surface)
* [Fonction Imc](/activité/Fct_imc)
* [Helloworld](/activité/HelloWorld_JS)
* [Suite Syracuse](/activité/Suite_Syracuse)
* [Constructeur d'objet IMC](/activité/constructor_obj_imc)
* [Interpretation IMC](/activité/interpretation_imc)
* [Objet IMC](/activité/obj_imc)
* [Optimisation constructeur IMC](/activité/opti_construtor_imc)

## Exercices

* [Calcul IMC](/exercices/Calcul_Imc)
* [Conversion euros en dollars](/exercices/Euros_Dollars)
* [Nombre triples](/exercices/Nombres_triples)
* [Suite Fibonacci](/exercices/Suite_Fibonacci)
* [Table de multiplication](/exercices/Table_multiplication)
* [conversion °C en °F](/exercices/conversion_°C°F)
* [Factorielle](/exercices/factorielle)
* [Recherche multiple de 3](/exercices/recherche_multiple_3)
* [Calcul du temp de trajet](/exercices/temp_trajet)

# ressources 

* [GitLab Alain Brassart](https://gitlab.com/alain.brassart)
* [GitLab Gwenael Laurent](https://gitlab.com/gwenael.laurent)
